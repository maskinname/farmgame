var LandDataNotify = require('LandDataNotify');
cc.Class({
    extends: cc.Component,

    
    properties: {
        HighLight : cc.Node,
    },

    // use this for initialization
    onLoad: function () {
        cc.director.getCollisionManager().enabled = true;
        cc.director.getCollisionManager().enabledDebugDraw = true;
        // cc.director.getCollisionManager().enabledDrawBoundingBox = true;
        
    },

    initData(_data){
        this.data = _data
        this.setLocked(this.data.isLock)
        this.setName(this.data.name)
    },

    setLocked(_bool){
        this.node.getChildByName("Locked").active = _bool
    },

    setName(_data){
        cc.find("Background/Label",this.node).getComponent(cc.Label).string = _data
    },

    onClick(_Node){
        console.log("土地点击,是否上锁：",this.data.isLock)
    },
    
    onCollisionEnter: function (other) {
        //地块锁定直接返回
        if(this.data.isLock) return
        //显示高亮模块
        this.HighLight.active = true;
        //将高亮地块设置为true
        LandDataNotify.HighLightArray[this.data.Number] = true;
        console.log("onCollisionEnter:",LandDataNotify.HighLightArray)
    },
    
    onCollisionStay: function (other) {
        // console.log('on collision stay',other);
    },
    
    onCollisionExit: function () {
        //隐藏高亮
        this.HighLight.active = false;
        //将地块高亮设置为false
        LandDataNotify.HighLightArray[this.data.Number] = false;
        console.log("onCollisionExit:",LandDataNotify.HighLightArray)
    }

    // update: function (dt) {

    // },
});
