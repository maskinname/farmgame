var LobbyDataNotify = require("LobbyDataNotify")
var CommonConst = require("CommonConst")
cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    initDrawing(_data){
        cc.find("Background/Name",this.node).getComponent(cc.Label).string = _data.name
        this.node.getChildByName("NumTip").active = !_data.isLock;
        this.node.getChildByName("LockTip").active = _data.isLock;
        this.node.getChildByName("Lock").active = _data.isLock;
        if(_data.isLock){
            this.node.getChildByName("LockTip").getComponent(cc.Label).string = _data.name;
        }else{
            cc.find("NumTip/Num",this.node).getComponent(cc.Label).string = _data.DrawingNumber;
        }


    },

    onClick(_Node){
        console.log("图集点击",_Node.currentTarget._name);
        LobbyDataNotify.CurrentTarget = _Node.currentTarget._name;
        LobbyDataNotify.EventIsMove = _Node.currentTarget;
    }

    // update (dt) {},
});
