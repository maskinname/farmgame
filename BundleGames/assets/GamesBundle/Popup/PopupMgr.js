var BaseLayer = require('BaseLayer');
var PopupDataNotify = require('PopupDataNotify');
cc.Class({
    extends: BaseLayer,

    properties: {
        CurrentPopups : [],
    },

    PreCustomerInit()
    {
        this.InitPopup()
        PopupDataNotify.AddListener("LoadPopup" , this.onLoadPopup , this);
        PopupDataNotify.AddListener("PopupBGShow" , this.onPopupBGShow , this);
    },

    CustomerInit()
    {
    },

    CustomerRelease()
    {
        PopupDataNotify.RemoveListenerByTarget(this);
    },

    InitPopup(){
        console.log("初始化Popup")
        this.BGNode = this.node.getChildByName("BG")
        this.BGNode.active = false
    },
    /**
     * @param {*} _data 
     * {
     *      Name : "",
     *      ShowType : true,
     * }
     */
    onLoadPopup(_data){
        if(PopupDataNotify.CurrentPopups){
            for (let index = 0; index < PopupDataNotify.CurrentPopups.length; index++) {
                var tempNode = PopupDataNotify.CurrentPopups[index];
                if(tempNode.name = _data.Name){
                    console.log("缓存加载节点",_data.Name)
                    var Script = tempNode.getComponent(_data.Name)
                    Script.Show(this.node)
                    return
                }
            }
        }

        if(this.m_Prefabs.length == 0){
            console.log("预制体仓库为空")
            return
        }
        for (let index = 0; index < this.m_Prefabs.length; index++) {
            var Prefab = this.m_Prefabs[index];
            if(Prefab.name = _data.Name){
                console.log("instantiate节点：",_data.Name)
                var tempNode = cc.instantiate(Prefab);
                if(!PopupDataNotify.CurrentPopups){
                    PopupDataNotify.CurrentPopups = []
                }
                //放入当前弹窗池
                PopupDataNotify.CurrentPopups.push(tempNode)
                var Script = tempNode.getComponent(_data.Name)
                Script.Show(this.node)
                return
            }
        }
    },

    onPopupBGShow(_data){
        this.BGNode.active = _data
    },

    testClick(){

    }
});
