var HttpMgr = require('HttpMgr');
var AppConfig = require("AppConfig")
var HotUpdateMgr = require("HotUpdateMgr")
var GameConst = require("GameConst")
var UIUtil = require("UIUtil")
cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.initGame()
    },
    
    initGame(){
        if(AppConfig.BuildType == GameConst.BuildType.Debug){
            UIUtil.InitGameBundle(GameConst.GameBundleFloder)
        }else{
            this.hotUpdate()
        }
    },

    hotUpdate(){
        // this.LoadBundle()
        HotUpdateMgr.init();
        HotUpdateMgr.CheckUpdate()
    },
    // update (dt) {},
});
